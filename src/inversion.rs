use attheme::{Attheme, ColorSignature::Hex};
use image::{
    DynamicImage,
    ImageOutputFormat::{JPEG, PNG},
};
use palette::{Hsl, Hue, Srgb};
use tdesktop_theme::{TdesktopTheme, Value::Color, WallpaperExtension};

#[derive(Clone, Copy)]
pub enum InvertMode {
    Rgb,
    HslLightness,
    HslShift,
}

pub enum Theme {
    Attheme(Attheme),
    TdesktopTheme(TdesktopTheme),
}

impl Theme {
    pub fn to_bytes(&self) -> Option<Vec<u8>> {
        match self {
            Theme::Attheme(theme) => Some(theme.to_bytes(Hex)),
            Theme::TdesktopTheme(theme) => match theme.to_zip_bytes() {
                Ok(theme) => Some(theme),
                Err(error) => {
                    dbg!(error);
                    None
                }
            },
        }
    }
}

fn invert_color(mut color: [u8; 3], mode: InvertMode) -> [u8; 3] {
    let red = color[0] as f32 / 255.0;
    let green = color[1] as f32 / 255.0;
    let blue = color[2] as f32 / 255.0;

    let rgb = match mode {
        InvertMode::Rgb => Srgb::new(1.0 - red, 1.0 - green, 1.0 - blue),
        InvertMode::HslLightness => {
            let mut hsl: Hsl = Srgb::new(red, green, blue).into();
            hsl.lightness = 1.0 - hsl.lightness;
            Srgb::from(hsl)
        }
        InvertMode::HslShift => {
            let hsl: Hsl = Srgb::new(red, green, blue).into();
            Srgb::from(hsl.shift_hue(180.0))
        }
    };

    color[0] = (rgb.red * 255.0) as u8;
    color[1] = (rgb.green * 255.0) as u8;
    color[2] = (rgb.blue * 255.0) as u8;

    color
}

pub fn invert_theme(theme: &mut Theme, mode: InvertMode) {
    match theme {
        Theme::Attheme(theme) => invert_attheme(theme, mode),
        Theme::TdesktopTheme(theme) => invert_tdesktop_theme(theme, mode),
    }
}

pub fn invert_tdesktop_theme(theme: &mut TdesktopTheme, mode: InvertMode) {
    for (.., value) in theme.variables_mut() {
        let [red, green, blue, alpha] = match value {
            Color(color) => color,
            _ => continue,
        };
        let [red, green, blue] = invert_color([*red, *green, *blue], mode);
        *value = Color([red, green, blue, *alpha]);
    }

    let wallpaper = match &mut theme.wallpaper {
        Some(wallpaper) => wallpaper,
        None => return,
    };

    let mut image = match image::load_from_memory(&wallpaper.bytes) {
        Ok(image) => image.into_rgba(),
        Err(..) => return,
    };

    for pixel in image.pixels_mut() {
        let red = pixel[0];
        let green = pixel[1];
        let blue = pixel[2];

        let [red, green, blue] = invert_color([red, green, blue], mode);
        pixel[0] = red;
        pixel[1] = green;
        pixel[2] = blue;
    }

    let image = DynamicImage::ImageRgba8(image);

    wallpaper.bytes.clear();

    match wallpaper.extension {
        WallpaperExtension::Jpg => {
            if let Err(error) = image.write_to(&mut wallpaper.bytes, JPEG(255))
            {
                eprintln!("Error while writing to wallpaper: {:#?}", error);
            }
        }
        WallpaperExtension::Png => {
            if let Err(error) = image.write_to(&mut wallpaper.bytes, PNG) {
                eprintln!("Error while writing to wallpaper: {:#?}", error);
            }
        }
    }
}

pub fn invert_attheme(theme: &mut Attheme, mode: InvertMode) {
    for (.., color) in theme.variables.iter_mut() {
        let (red, green, blue, alpha) = color.into_components();
        let [red, green, blue] = invert_color([red, green, blue], mode);
        *color = (red, green, blue, alpha).into();
    }

    let mut wallpaper = match &mut theme.wallpaper {
        Some(wallpaper) => wallpaper,
        None => return,
    };
    let mut image = match image::load_from_memory(wallpaper) {
        Ok(image) => image,
        Err(..) => return,
    };
    let image_rgb8 = match image.as_mut_rgb8() {
        Some(image) => image,
        None => return,
    };

    for pixel in image_rgb8.pixels_mut() {
        let red = pixel[0];
        let green = pixel[1];
        let blue = pixel[2];

        let [red, green, blue] = invert_color([red, green, blue], mode);
        pixel[0] = red;
        pixel[1] = green;
        pixel[2] = blue;
    }

    wallpaper.clear();
    if let Err(error) = image.write_to(&mut wallpaper, JPEG(255)) {
        eprintln!("Error while writing to wallpaper: {:#?}", error);
    }
}
