use attheme::{default_themes::default as attheme_default, Attheme, FALLBACKS};
use dotenv::dotenv;
use std::{borrow::Cow, env, path::Path, sync::Arc};
use tbot::{
    contexts,
    prelude::*,
    types::{
        callback::Origin,
        input_file::Document,
        keyboard::inline::{Button, ButtonKind::CallbackData},
        message, update,
    },
    Bot,
};
use tdesktop_theme::{
    default_themes::classic as tdesktop_theme_default, TdesktopTheme,
};

mod inversion;
mod localization;

use inversion::{invert_theme, InvertMode, Theme};
use localization::*;

const SUPPORTED_EXTENSIONS: [&str; 3] =
    ["attheme", "tdesktop-theme", "tdesktop-palette"];

#[tokio::main]
async fn main() {
    let _ = dotenv();
    let mut bot = Bot::from_env("BOT_TOKEN").event_loop();

    bot.start(help_handler);
    bot.help(help_handler);
    bot.document(document);
    bot.data_callback(data_callback);
    bot.unhandled(unhandled);

    if let Ok(url) = env::var("WEBHOOK_URL") {
        let port = env::var("WEBHOOK_PORT")
            .ok()
            .and_then(|x| x.parse().ok())
            .expect("Specify WEBHOOK_PORT");
        bot.webhook(&url, port).http().start().await.unwrap();
    } else {
        bot.polling().start().await.unwrap();
    }
}

async fn help_handler(context: Arc<contexts::Command<contexts::Text>>) {
    let message = help_message(&context.from);
    let call_result = context.send_message(message).call().await;
    if let Err(err) = call_result {
        dbg!(err);
    }
}

async fn document(context: Arc<contexts::Document>) {
    let filename = match &context.document.file_name {
        Some(name) => name,
        None => return,
    };
    let extension = match Path::new(&filename).extension() {
        Some(extension) => extension.to_string_lossy(),
        None => Cow::Borrowed(""),
    };

    if !SUPPORTED_EXTENSIONS.contains(&&*extension) {
        let call_result = context
            .send_message(unknown_file(&context.from))
            .call()
            .await;

        if let Err(err) = call_result {
            dbg!(err);
        }
        return;
    }

    let keyboard: &[&[Button]] = &[
        &[Button::new(
            lightness_inversion(&context.from),
            CallbackData("l"),
        )],
        &[Button::new(hue_inversion(&context.from), CallbackData("h"))],
        &[Button::new(rgb_inversion(&context.from), CallbackData("r"))],
    ];

    let call_result = context
        .send_message_in_reply(choose_inversion_mode(&context.from))
        .reply_markup(keyboard)
        .call()
        .await;

    if let Err(err) = call_result {
        dbg!(err);
    }
}

async fn data_callback(context: Arc<contexts::DataCallback>) {
    let context_ignore = Arc::clone(&context);
    tokio::spawn(async move {
        let call_result = context_ignore.ignore().call().await;
        if let Err(err) = call_result {
            dbg!(err);
        }
    });

    let mode = match context.data.as_str() {
        "r" => InvertMode::Rgb,
        "l" => InvertMode::HslLightness,
        "h" => InvertMode::HslShift,
        _ => return,
    };

    let message = match &context.origin {
        Origin::Message(message) => message,
        _ => return,
    };
    let chat = message.chat.id;
    let id = message.id;
    let reply_to = match &message.reply_to {
        Some(reply_to) => reply_to,
        None => return,
    };
    let document = match &reply_to.kind {
        message::Kind::Document(document, ..) => document,
        _ => return,
    };
    let file_name = match &document.file_name {
        Some(file_name) => file_name,
        None => return,
    };
    let path = Path::new(file_name);
    let mut extension = match path.extension() {
        Some(extension) => extension.to_string_lossy(),
        None => Cow::Borrowed(""),
    };

    if !SUPPORTED_EXTENSIONS.contains(&&*extension) {
        let result = context
            .bot
            .edit_message_text(chat, id, unknown_file(&reply_to.from))
            .call()
            .await;
        if let Err(err) = result {
            dbg!(err);
        }
        return;
    }

    if &extension == "tdesktop-palette" {
        extension = Cow::Borrowed("tdesktop-theme");
    }

    // We can unwrap it because we checked before that the file have extension
    // i.e. it has a dot already. The stem is guaranteed
    // to be returned in this case.
    let name = path.file_stem().unwrap().to_string_lossy();
    let name: String = name.chars().rev().collect();
    let filename = format!("{}.{}", name, extension);

    let context_delete = Arc::clone(&context);
    tokio::spawn(async move {
        let result = context_delete.bot.delete_message(chat, id).call().await;
        if let Err(err) = result {
            dbg!(err);
        }
    });

    let file = match context.bot.get_file(&**document).call().await {
        Ok(file) => file,
        Err(err) => {
            dbg!(err);
            return;
        }
    };
    let bytes = match context.bot.download_file(&file).await {
        Ok(bytes) => bytes,
        Err(err) => {
            dbg!(err);
            return;
        }
    };

    let mut theme = match &*extension {
        "attheme" => {
            let mut theme = Attheme::from_bytes(&bytes);
            theme.fallback_to_other(attheme_default());
            theme.fallback_to_self(FALLBACKS);
            if theme.wallpaper.is_some() {
                theme.variables.remove("chat_wallpaper");
                theme.variables.remove("chat_wallpaper_gradient_to");
            }

            Theme::Attheme(theme)
        }
        "tdesktop-theme" => {
            let mut theme = match TdesktopTheme::from_bytes(&bytes) {
                Ok(theme) => theme,
                Err(err) => {
                    dbg!(err);
                    let result = context
                        .bot
                        .send_message(chat, invalid_theme(&reply_to.from))
                        .call()
                        .await;
                    if let Err(err) = result {
                        dbg!(err);
                    }
                    return;
                }
            };
            theme |= tdesktop_theme_default();

            Theme::TdesktopTheme(theme)
        }
        _ => unreachable!(),
    };

    invert_theme(&mut theme, mode);
    let bytes = match theme.to_bytes() {
        Some(bytes) => bytes,
        None => return,
    };
    let document =
        Document::with_bytes(&filename, &bytes).caption(result(&reply_to.from));
    let result = context
        .bot
        .send_document(chat, document)
        .in_reply_to(reply_to.id)
        .call()
        .await;
    if let Err(err) = result {
        dbg!(err);
    }
}

async fn unhandled(context: Arc<contexts::Unhandled>) {
    let message = match &context.update {
        update::Kind::Message(message) => message,
        _ => return,
    };

    let result = context
        .bot
        .send_message(message.chat.id, unknown_message_kind(&message.from))
        .call()
        .await;

    if let Err(err) = result {
        dbg!(err);
    }
}
