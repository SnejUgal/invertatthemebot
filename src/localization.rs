use lazy_static::lazy_static;
use tbot::{
    markup::{inline_code, markdown_v2},
    types::{parameters::Text, User},
};

fn get_language(user: &Option<User>) -> Option<&str> {
    match user {
        Some(User {
            language_code: Some(language),
            ..
        }) => Some(language.as_str()),
        _ => None,
    }
}

pub fn help_message(user: &Option<User>) -> Text<'_> {
    lazy_static! {
        static ref ENGLISH: String = markdown_v2((
            "Hello! I'm a bot that makes light themes dark and dark themes \
             light. Just send me an ",
            inline_code([".attheme"]),
            " or ",
            inline_code([".tdesktop-theme"]),
            "!"
        ))
        .to_string();
        static ref RUSSIAN: String = markdown_v2((
            "Привет! Я — бот, который делает тёмные темы из светлых и светлые \
             темы из тёмных. Просто отправь мне ",
            inline_code([".attheme"]),
            " мне ",
            inline_code([".tdesktop-theme"]),
            " тему!"
        ))
        .to_string();
    }

    match get_language(user) {
        Some("ru") => Text::with_markdown_v2(&RUSSIAN),
        _ => Text::with_markdown_v2(&ENGLISH),
    }
}

pub fn choose_inversion_mode(user: &Option<User>) -> Text<'_> {
    match get_language(user) {
        Some("ru") => Text::with_plain("Выбери режим инверсирования:"),
        _ => Text::with_plain("Choose inversion mode:"),
    }
}

pub fn lightness_inversion(user: &Option<User>) -> &str {
    match get_language(user) {
        Some("ru") => "Инверсия светлости",
        _ => "Lightness inversion",
    }
}

pub fn hue_inversion(user: &Option<User>) -> &str {
    match get_language(user) {
        Some("ru") => "Инверсия тона",
        _ => "Hue inversion",
    }
}

pub fn rgb_inversion(user: &Option<User>) -> &str {
    match get_language(user) {
        Some("ru") => "Инверсия по RGB",
        _ => "RGB inversion",
    }
}

pub fn result(user: &Option<User>) -> Text<'_> {
    match get_language(user) {
        Some("ru") => Text::with_plain("Вот инверсированная тема!"),
        _ => Text::with_plain("Here's your inverted theme!"),
    }
}

pub fn unknown_file(user: &Option<User>) -> Text<'_> {
    lazy_static! {
        static ref ENGLISH: String = markdown_v2((
            "I don't know what this file is :( As of now, \
             I can only work with ",
            inline_code([".attheme"]),
            " and ",
            inline_code([".tdesktop-theme"]),
            " files.",
        ))
        .to_string();
        static ref RUSSIAN: String = markdown_v2((
            "Я не знаю, что это за файл :( Сейчас я умею работать только с ",
            inline_code([".attheme"]),
            " и ",
            inline_code([".tdesktop-theme"]),
            " файлами.",
        ))
        .to_string();
    }

    match get_language(user) {
        Some("ru") => Text::with_markdown_v2(&RUSSIAN),
        _ => Text::with_markdown_v2(&ENGLISH),
    }
}

pub fn unknown_message_kind(user: &Option<User>) -> Text<'_> {
    lazy_static! {
        static ref ENGLISH: String = markdown_v2((
            "I can only work with ",
            inline_code([".attheme"]),
            " and ",
            inline_code([".tdesktop-theme"]),
            " files.",
        ))
        .to_string();
        static ref RUSSIAN: String = markdown_v2((
            "Я умею работать только с ",
            inline_code([".attheme"]),
            " и ",
            inline_code([".tdesktop-theme"]),
            " файлами.",
        ))
        .to_string();
    }

    match get_language(user) {
        Some("ru") => Text::with_markdown_v2(&RUSSIAN),
        _ => Text::with_markdown_v2(&ENGLISH),
    }
}

pub fn invalid_theme(user: &Option<User>) -> Text<'_> {
    match get_language(user) {
        Some("ru") => {
            Text::with_plain("Тема невалидна :( Попробуй другую тему.")
        }
        _ => Text::with_plain("The theme is invalid :( Try another theme."),
    }
}
